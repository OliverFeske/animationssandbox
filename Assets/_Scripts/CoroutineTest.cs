﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{
	private WaitForSeconds wf1s;
	private Vector3 startPos;
	private Vector3 endPos;
	private float timer;
	private float duration = 2f;
	private bool canMoveUp;

	void Start()
	{
		startPos = transform.position;
		endPos = startPos + new Vector3(2f, 2f, 2f);
		wf1s = new WaitForSeconds(1f);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			StartCoroutine(MoveUp());
		}
	}

	IEnumerator MoveUp()
	{
		while (timer <= duration)
		{
			timer += Time.deltaTime;
			float t = timer / duration;
			transform.position = Vector3.Lerp(startPos, endPos, t);
			yield return null;          // break the loop every frame
		}
		yield return wf1s;
		timer = 0f;
		while (timer <= duration)
		{
			timer += Time.deltaTime;
			float t = timer / duration;
			transform.position = Vector3.Lerp(endPos, startPos, t);
			yield return null;          // break the loop every frame
		}
		timer = 0f;
	}
}
