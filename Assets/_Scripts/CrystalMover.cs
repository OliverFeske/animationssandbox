﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CrystalMover : MonoBehaviour, IPointerClickHandler
{
	[SerializeField] private Transform crystal;
	[SerializeField] private AnimationCurve myCurve;
	[SerializeField] private AnimationCurve onClickCurve;
	[SerializeField] private Color errorColor = Color.red;
	//[SerializeField] private EaseFunction easy;
	[SerializeField] private float duration;
	//[SerializeField] private float test = 0.5f;
	private GameObject nextGO;
	private Vector3 lastPos;
	private Vector3 nextPos;
	private Color invertedColor;
	private bool isTweening;

	void Start()
	{
		//easy = MyEase;
		lastPos = crystal.position;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		Renderer rend = eventData.pointerCurrentRaycast.gameObject.GetComponent<Renderer>();
		Color originColor = rend.material.color;
		Sequence seq = DOTween.Sequence();

		if (isTweening)
		{
			seq.Append(rend.material.DOColor(errorColor, 0.3f).SetEase(onClickCurve));
			seq.Play();
			return;
		}

		seq.Append(rend.material.DOColor(Color.blue, 0.3f).SetEase(onClickCurve));
		isTweening = true;
		DOTween.KillAll();
		nextGO = eventData.pointerCurrentRaycast.gameObject;
		nextPos = nextGO.transform.parent.position;

		MoveCrystal(nextPos);

		lastPos = nextPos;
	}

	void MoveCrystal(Vector3 nextPos)
	{
		float distanceFactor = (nextPos - lastPos).magnitude;

		crystal.DOMove(nextPos, duration * distanceFactor).SetEase(/*myCurve*/Ease.Linear).OnComplete(() => isTweening = false);
	}
}
